clear; clc;
% This is the 330-93 normal SR rhythm
d33093_normal = importdata('/Users/amal029_old/PatientEGMs/A330093.wav');
d33093_normal_ecg = (smooth(dtrend(d33093_normal.data(:, 3)), 20));
d33093_normal_vt = smooth(dtrend(d33093_normal.data(:, 7)), 20);

i = 10000;
j = i*2;
train_input = d33093_normal_vt(1:i);
test_input = d33093_normal_vt(i+1:j);

net = newrb(train_input, train_input, 1.0);
view(net);
ycap = net(test_input);
plot([ycap, test_input]);
title(corr(ycap, test_input));