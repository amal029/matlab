clear; clc;

% This is the 343330 vft
d343300 = importdata('../PatientEGMs/A343250.wav');
d343300_ecg = detrend(smooth((d343300.data(:, 1)), 5));
d343300_vt = detrend(smooth((d343300.data(:, 7)), 5));

%176230 vft
d176230 = importdata('../PatientEGMs/A176230.wav');
d176230_ecg = detrend(smooth((d176230.data(:, 2)), 5));
d176230_vt = detrend(smooth((d176230.data(:, 7)), 5));

%198a385 vft
d198a385 = importdata('../PatientEGMs/A198a85.wav');
d198a385_ecg = detrend(smooth(d198a385.data(:, 2), 5));
d198a385_vt = detrend(smooth(d198a385.data(:, 7), 5));

% Only 3000 samples of vft.
s = 3000;
e = 6000;

train_input = dtrend([d343300_ecg(s:e); d176230_ecg(s:e)]);
train_output = dtrend([d343300_vt(s:e); d176230_vt(s:e)]);

MAX = 20;
algos = ["trainlm", "trainscg"];
asize = size(algos);
neurons = [5:5:20, 40, 80, 100];
nsize = size(neurons);
results_vft = cell(MAX*asize(2)*nsize(2),1);
fr1_vft = cell(MAX*asize(2)*nsize(2),1);
fr2_vft = cell(MAX*asize(2)*nsize(2),1);
fr3_vft = cell(MAX*asize(2)*nsize(2),1);
max_avg = 0.0;
count = 1;
for n = 1:nsize(2)
    for delay = 1:MAX
        for a = 1:asize(2)
            [X, ~] = tonndata(train_input, false, false);
            [T, ~] = tonndata(train_output, false, false);
            net = timedelaynet((0:delay), neurons(n), char(algos(a)));
            net.trainParam.showWindow = false;
            net.trainParam.showCommandLine = true;
            [Xs,Xi,Ai,Ts] = preparets(net,X,T);
            net = train(net,Xs,Ts, Xi, Ai);
            % compute correlation coefficients.
            Ycap343300 = cell2mat(transpose(net(tonndata(d343300_ecg(s:e), false, false), tonndata(d343300_ecg(s:s+(delay-1)), false, false))));
            C343300 = corr(Ycap343300, d343300_vt(s:e));
            
            Ycap176230 = cell2mat(transpose(net(tonndata(d176230_ecg(s:e), false, false), tonndata(d176230_ecg(s:s+(delay-1)), false, false))));
            C176230 = corr(Ycap176230, d176230_vt(s:e));
            
            Ycap198a385 = cell2mat(transpose(net(tonndata(d198a385_ecg(10000:15000), false, false), tonndata(d198a385_ecg(10000:10000+(delay-1)), false, false))));
            C198a385 = corr(Ycap198a385, d198a385_vt(10000:15000));
            avg = (C343300+C176230+C198a385)/3.0;
            if avg > max_avg
                max_avg = avg;
            end
            results_vft(count) = {[avg, C343300, C176230, C198a385, neurons(n), delay, a]};
            fr1_vft(count) = {transpose(Ycap343300)};
            fr2_vft(count) = {transpose(Ycap176230)};
            fr3_vft(count) = {transpose(Ycap198a385)};
            count = count + 1;
        end
    end
end
% Plotting one of the better ones.
% 
best = 35;
figure();
subplot(3, 1, 1);
% % Validate
plot([transpose(fr1_vft{best,1}), d343300_vt(s:e)]);
t = '343300 training vec 1: ' + string(corr(transpose(fr1_vft{best,1}), d343300_vt(s:e)));
title(t);

subplot(3, 1, 2);
% Validate
plot([transpose(fr2_vft{best,1}), d176230_vt(s:e)]);
t = '176230, training vec 2: ' + string(corr(transpose(fr2_vft{best,1}), d176230_vt(s:e)));
title(t);

subplot(3, 1, 3);
%Test new input
plot([transpose(fr3_vft{best, 1}), d198a385_vt(10000:15000)]);
t = '198a385,never seen before: ' + string(corr(transpose(fr3_vft{best, 1}), d198a385_vt(10000:15000)));
title(t);
