% Prediction using radial basis function nueral net.
clear; clc;

% This is the 330-93 normal SR rhythm
d33093_normal = importdata('/Users/amal029_old/PatientEGMs/A330093.wav');
d33093_normal_ecg = (smooth(dtrend(d33093_normal.data(:, 3)),5));
d33093_normal_vt = smooth(dtrend(d33093_normal.data(:, 7)), 5);

% This is the 343220 normal SR rhythm
d343220_normal = importdata('/Users/amal029_old/PatientEGMs/A343220.wav');
d343220_normal_ecg = (smooth(dtrend(d343220_normal.data(:, 2)), 5));
d343220_normal_vt = (smooth(dtrend(d343220_normal.data(:, 7)), 5));

d228sr = importdata('/Users/amal029_old/PatientEGMs/A228a68.wav');
d228sr_ecg = (smooth(dtrend(d228sr.data(:, 2)), 5));
d228sr_vt = (smooth(dtrend(d228sr.data(:, 7)), 5));

d221sr = importdata('/Users/amal029_old/PatientEGMs/A221708.wav');
d221sr_ecg = (smooth(dtrend(d221sr.data(:, 2)), 5));
d221sr_vt = (smooth(dtrend(d221sr.data(:, 7)), 5));

%plot_new(1000,5000, d33093_normal_ecg, d33093_normal_vt, d343220_normal_ecg, d343220_normal_vt, d221sr_ecg, d221sr_vt);

i = 10000;
j = i*2;
train_input = transpose(dtrend([d343220_normal_ecg(1:i); d33093_normal_ecg(1:i)]));
train_output = transpose(dtrend([d343220_normal_vt(1:i); d33093_normal_vt(1:i)]));

%train_input = d343220_normal_ecg(1:i);
%train_output = d343220_normal_vt(1:i);

% Train the nueral network
net = newrb(train_input, train_output, 0.002);
%net = fitnet(20);
view(net);
%[net, tr] = train(net, train_input, train_output);
%view(net);
%nntraintool;
%hold on;
s = 1;
e = 6000;
%Testing
test_input = transpose(dtrend([d343220_normal_ecg(i+1:j); d33093_normal_ecg(i+1:j)]));
test_output = (dtrend([d343220_normal_vt(i+1:j); d33093_normal_vt(i+1:j)]));

% figure();
% subplot(2, 1, 1);
% plot(train_input);
% subplot(2, 1, 2);
% plot(test_input);

% Validation
y = transpose(net(test_input));
figure();
%subplot(3, 1, 2);
plot([y(s:e,1), test_output(s:e,1)]);
title(corr(y(s:e), test_output(s:e)));
%subplot(3, 1, 3);
%plot(test_input(s:e));
%subplot(3, 1, 1);
%plot(train_input(s:e));

% Test new input set.
ks=1;
ke=size(d221sr_vt);
y221cap = transpose(net(transpose(d221sr_ecg(ks:ke))));
figure();
plot([y221cap, d221sr_vt(ks:ke)]);
title(corr(y221cap, d221sr_vt(ks:ke)));


% The driver for chaning the spread -- TODO.
