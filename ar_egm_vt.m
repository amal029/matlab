clear; clc;

% This is the 343330 vft
d343300 = importdata('/Users/amal029_old/PatientEGMs/A343250.wav');
d343300_ecg = detrend(smooth((d343300.data(:, 1)), 5));
d343300_vt = detrend(smooth((d343300.data(:, 7)), 5));

%176230 vft
d176230 = importdata('/Users/amal029_old/PatientEGMs/A176230.wav');
d176230_ecg = detrend(smooth((d176230.data(:, 2)), 5));
d176230_vt = detrend(smooth((d176230.data(:, 7)), 5));

%198a385 vft
d198a385 = importdata('/Users/amal029_old/PatientEGMs/A198a85.wav');
d198a385_ecg = detrend(smooth(d198a385.data(:, 2), 5));
d198a385_vt = detrend(smooth(d198a385.data(:, 7), 5));

% Only 3000 samples of vft.
s = 3000;
e = 6000;

d343300_vt_tr = d343300_vt(s:e);
d176230_vt_te = d176230_vt(s:e);
d198a385_vt_te = d198a385_vt(10000:15000);
d343300_ecg_te = d343300_ecg(s:e);

figure();
subplot(3, 1, 1);
ycap343300 = egm_egm_ar(d343300_vt_tr', transpose(d343300_vt_tr(1:10)))';
plot([ycap343300, d343300_vt_tr]);
title ('Never seen before, 343300 vft egm: ' + string(corr(ycap343300, d343300_vt_tr)));
subplot(3, 1, 3);
ycap176 = egm_egm_ar(d176230_vt_te', transpose(d176230_vt_te(1:10)))';
plot([ycap176, d176230_vt_te]);
title ('Never seen before, 176230 vft egm: ' + string(corr(ycap176, d176230_vt_te)));

subplot(3, 1, 2);
ycap343300 = egm_egm_ar(d343300_ecg_te', transpose(d343300_ecg_te(1:10)))';
plot([ycap343300, d343300_ecg_te]);
title ('Never seen before, 343300 vft ecg: ' + string(corr(ycap343300, d343300_ecg_te)));