function plot_vt(s, e, vt1, vt2, vt3, c1, c2, c3, nc1, nc2, nc3)
subplot(3, 1, 1);
plot(vt1(s:e,[c1, nc1]));
subplot(3, 1, 2);
plot(vt2(s:e,[c2, nc2]));
subplot(3, 1, 3);
plot(vt3(s:e,[c3, nc3]));