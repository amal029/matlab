% 5 neurons, 12 delay
clear; clc;

% This is the 343330 vft
d343300 = importdata('/Users/amal029_old/PatientEGMs/A343250.wav');
d343300_ecg = detrend(smooth((d343300.data(:, 1)), 5));
d343300_vt = detrend(smooth((d343300.data(:, 7)), 5));

%176230 vft
d176230 = importdata('/Users/amal029_old/PatientEGMs/A176230.wav');
d176230_ecg = detrend(smooth((d176230.data(:, 2)), 5));
d176230_vt = detrend(smooth((d176230.data(:, 7)), 5));

%198a385 vft
d198a385 = importdata('/Users/amal029_old/PatientEGMs/A198a85.wav');
d198a385_ecg = detrend(smooth(d198a385.data(:, 2), 5));
d198a385_vt = detrend(smooth(d198a385.data(:, 7), 5));

% Only 3000 samples of vft.
s = 3000;
e = 6000;
% figure();
% subplot(2, 1, 1);
% plot(d343300_ecg(s:e));
% title('ecg');
% subplot(2, 1, 2);
% plot(d343300_vt(s:e));
% title('vt');
% 
% figure();
% subplot(2, 1, 1);
% plot(d176230_ecg(s:e));
% title('ecg');
% subplot(2, 1, 2);
% plot(d176230_vt(s:e));
% title('vt');
% 
% figure();
% subplot(2, 1, 1);
% plot(d198a385_ecg(10000:15000));
% title('ecg');
% subplot(2, 1, 2);
% plot(d198a385_vt(10000:15000));
% title('vt');
% 
% 
% train_input = dtrend([d176230_ecg(s:e); d198a385_ecg(10000:15000)]);
% train_output = dtrend([d176230_vt(s:e); d198a385_vt(10000:15000)]);
%train_input = dtrend([d176230_ecg(s:e)]);
%train_output = dtrend([d176230_vt(s:e)]);
% figure();
% subplot(2, 1, 1);
% plot(train_input);
% subplot(2, 1, 2);
% plot(train_output);
% 
% test_input = d343300_ecg(s:e);
% test_output = d343300_vt(s:e);

figure();
subplot(3, 1, 1);
% % Validate
Ycap343300 = transpose(vft_three(transpose(d343300_ecg(s:e)), transpose(d343300_ecg(s:s+15))));
plot([Ycap343300, d343300_vt(s:e)]);
t = '343300 , never seen before: ' + string(corr(Ycap343300, d343300_vt(s:e)));
title(t);

subplot(3, 1, 2);
% Validate
Ycap176230 = transpose(vft_three(transpose(d176230_ecg(s:e)), transpose(d176230_ecg(s:s+15))));
plot([Ycap176230, d176230_vt(s:e)]);
t = '176230, seen in training vector: ' + string(corr(Ycap176230, d176230_vt(s:e)));
title(t);

subplot(3, 1, 3);
%Test new input
Ycap198a385 = transpose(vft_three(transpose(d198a385_ecg(10000:15000)), transpose(d198a385_ecg(10000:10015))));
plot([Ycap198a385, d198a385_vt(10000:15000)]);
t = '198a385,never seen before: ' + string(corr(Ycap198a385, d198a385_vt(10000:15000)));
title(t);