function [Y,Xf,Af] = nnecg_rva_normal(X,Xi,~)
%MYNEURALNETWORKFUNCTION neural network simulation function.
%
% Generated by Neural Network Toolbox function genFunction, 21-Nov-2017 18:13:50.
%
% [Y,Xf,Af] = myNeuralNetworkFunction(X,Xi,~) takes these arguments:
%
%   X = 1xTS cell, 1 inputs over TS timesteps
%   Each X{1,ts} = 1xQ matrix, input #1 at timestep ts.
%
%   Xi = 1x1 cell 1, initial 1 input delay states.
%   Each Xi{1,ts} = 1xQ matrix, initial states for input #1.
%
%   Ai = 2x0 cell 2, initial 1 layer delay states.
%   Each Ai{1,ts} = 20xQ matrix, initial states for layer #1.
%   Each Ai{2,ts} = 1xQ matrix, initial states for layer #2.
%
% and returns:
%   Y = 1xTS cell of 1 outputs over TS timesteps.
%   Each Y{1,ts} = 1xQ matrix, output #1 at timestep ts.
%
%   Xf = 1x1 cell 1, final 1 input delay states.
%   Each Xf{1,ts} = 1xQ matrix, final states for input #1.
%
%   Af = 2x0 cell 2, final 0 layer delay states.
%   Each Af{1ts} = 20xQ matrix, final states for layer #1.
%   Each Af{2ts} = 1xQ matrix, final states for layer #2.
%
% where Q is number of samples (or series) and TS is the number of timesteps.

%#ok<*RPMT0>

% ===== NEURAL NETWORK CONSTANTS =====

% Input 1
x1_step1.xoffset = -0.0163571239227562;
x1_step1.gain = 48.9620663940341;
x1_step1.ymin = -1;

% Layer 1
b1 = [-21.881183404120172042;-19.248199121458309691;-17.122437117731035272;-14.928232237751206668;-12.053796158022288765;-10.808988196969766094;-8.130603513105643998;-5.2510053173410273786;4.1825801846846504262;1.0745153589076374967;-0.12752989541804346518;4.4041407001141301691;-7.4410754885796270841;-8.5710909399102259698;-10.174102233255810646;-12.922878259675952606;15.940604559161004161;-17.376226488046018659;-19.148133236002959023;-20.404626783511606192];
IW1_1 = [21.529090043600344728;21.850053376215097245;21.691030461419213538;21.635879682075206176;21.874494728758911322;21.465657110197771118;21.738141755468987526;21.898644165940766015;-22.239082483559819536;-21.59016572929852984;22.40019447797833152;22.7973453633958556;-20.219481753258136791;-23.657893330190056247;-23.002231579956518459;-21.613825742139145092;21.041123143813937446;-21.562571160270366022;-22.208661968606154602;-22.933006985833330305];

% Layer 2
b2 = -0.1796562195772575532;
LW2_1 = [-0.2974411082615919244 -0.0082989532544489076626 -0.0084915698436590792558 -0.0029939792415133036248 0.044985246224044206365 -0.016665493973046778542 -0.0029994973858658091972 0.045240970536801747293 0.26904524373408944538 -0.02389742472798095374 -0.015056901659017213335 -0.052822618699197645964 1.3834828822280615945 -1.2074371176947049644 -0.2536784723758401805 -0.02734665486942934598 0.19728019220862297312 0.6777050934379069691 -1.1922740104129805339 0.52077742756287459525];

% Output 1
y1_step1.ymin = -1;
y1_step1.gain = 76.3031493993439;
y1_step1.xoffset = -0.0210973751047318;

% ===== SIMULATION ========

% Format Input Arguments
isCellX = iscell(X);
if ~isCellX, X = {X}; end;
if (nargin < 2), error('Initial input states Xi argument needed.'); end

% Dimensions
TS = size(X,2); % timesteps
if ~isempty(X)
    Q = size(X{1},2); % samples/series
elseif ~isempty(Xi)
    Q = size(Xi{1},2);
else
    Q = 0;
end

% Input 1 Delay States
Xd1 = cell(1,2);
for ts=1:1
    Xd1{ts} = mapminmax_apply(Xi{1,ts},x1_step1);
end

% Allocate Outputs
Y = cell(1,TS);

% Time loop
for ts=1:TS
    
    % Rotating delay state position
    xdts = mod(ts+0,2)+1;
    
    % Input 1
    Xd1{xdts} = mapminmax_apply(X{1,ts},x1_step1);
    
    % Layer 1
    tapdelay1 = cat(1,Xd1{mod(xdts-1-1,2)+1});
    a1 = tansig_apply(repmat(b1,1,Q) + IW1_1*tapdelay1);
    
    % Layer 2
    a2 = repmat(b2,1,Q) + LW2_1*a1;
    
    % Output 1
    Y{1,ts} = mapminmax_reverse(a2,y1_step1);
end

% Final Delay States
finalxts = TS+(1: 1);
xits = finalxts(finalxts<=1);
xts = finalxts(finalxts>1)-1;
Xf = [Xi(:,xits) X(:,xts)];
Af = cell(2,0);

% Format Output Arguments
if ~isCellX, Y = cell2mat(Y); end
end

% ===== MODULE FUNCTIONS ========

% Map Minimum and Maximum Input Processing Function
function y = mapminmax_apply(x,settings)
y = bsxfun(@minus,x,settings.xoffset);
y = bsxfun(@times,y,settings.gain);
y = bsxfun(@plus,y,settings.ymin);
end

% Sigmoid Symmetric Transfer Function
function a = tansig_apply(n,~)
a = 2 ./ (1 + exp(-2*n)) - 1;
end

% Map Minimum and Maximum Output Reverse-Processing Function
function x = mapminmax_reverse(y,settings)
x = bsxfun(@minus,y,settings.ymin);
x = bsxfun(@rdivide,x,settings.gain);
x = bsxfun(@plus,x,settings.xoffset);
end
