clear; clc;

% This is the 343330 vft
d343300 = importdata('../PatientEGMs/A343250.wav');
d343300_ecg = detrend(smooth((d343300.data(:, 1)), 5));
d343300_vt = detrend(smooth((d343300.data(:, 7)), 5));

%176230 vft
d176230 = importdata('../PatientEGMs/A176230.wav');
d176230_ecg = detrend(smooth((d176230.data(:, 2)), 5));
d176230_vt = detrend(smooth((d176230.data(:, 7)), 5));

%198a385 vft
d198a385 = importdata('../PatientEGMs/A198a85.wav');
d198a385_ecg = detrend(smooth(d198a385.data(:, 2), 5));
d198a385_vt = detrend(smooth(d198a385.data(:, 7), 5));

% Only 3000 samples of vft.
s = 3000;
e1 = 5000;
e = 6000;

d343300_vt_tr = d343300_vt(s:e1);
d176230_vt_te = d176230_vt(s:e1);
d198a385_vt_te = d198a385_vt(10000:15000);
d343300_ecg_te = d343300_ecg(s:e);
d176230_ecg_te = d176230_ecg(s:e);
delay = 45;
net = narnet(1:delay, 10);
X1 = tonndata(d343300_ecg_te, false, false);
%T1 = tonndata(d343300_vt_tr, false, false);
T1 = tonndata(d176230_vt_te, false, false);

[Xs,Xi,Ai,Ts] = preparets(net,{},{},T1);
net.trainParam.showWindow = false;
net.trainParam.showCommandLine = true;
net = train(net,Xs,Ts,Xi,Ai);
%view(net);
figure();
subplot(2, 1, 1);
% Open loop simulation
[ycapo, Xf, Af] = net(Xs, Xi, Ai); % Just produces one next token, but does not use it for the next prediction.
ycapo = cell2mat(ycapo');
plot(ycapo);
title(corr(ycapo, cell2mat(T1(1:end-delay)')));
% Close loop
[netc, Xic, Aic] = closeloop(net, Xf, Af);
% prepare for the next dataset.
%X2 = tonndata(d176230_ecg_te, false, false);
%X3 = tonndata(d198a385_ecg(10000:15000), false, false);
%[Xs1, ~, ~, ~] = preparets(netc, T2, {}, {});
%view(netc);
ycap = cell2mat(netc(cell(0, e-e1+1), Xic, Aic)');
subplot(2, 1, 2);
plot(ycap);
title(corr(ycap, d176230_vt(e1:e)));

% Now with the same trained net, try to predict d198a385_vft
d198a385_vt_te1 = d198a385_vt(1:2000);
d198a385_vt_te2 = d198a385_vt_te(2001:5000);
T1 = tonndata(d198a385_vt_te1, false, false);

[Xs,Xi,Ai,Ts] = preparets(net,{},{},T1);

figure();
subplot(2, 1, 1);
% Open loop simulation
[ycapo, Xf, Af] = net(Xs, Xi, Ai); % Just produces one next token, but does not use it for the next prediction.
ycapo = cell2mat(ycapo');
plot(ycapo);
title(corr(ycapo, cell2mat(T1(1:end-delay)')));
% Close loop
[netc, Xic, Aic] = closeloop(net, Xf, Af);
ycap = cell2mat(netc(cell(0, 3000), Xic, Aic)');
subplot(2, 1, 2);
plot(ycap);
title(corr(ycap, d198a385_vt_te2));
