% Script to search for best timed delay neural net for prediction.
clear; clc;

% This is the 330-93 normal SR rhythm
d33093_normal = importdata('/Users/amal029_old/PatientEGMs/A330093.wav');
d33093_normal_ecg = (smooth(dtrend(d33093_normal.data(:, 3)),5));
d33093_normal_vt = smooth(dtrend(d33093_normal.data(:, 7)), 5);

% This is the 343220 normal SR rhythm
d343220_normal = importdata('/Users/amal029_old/PatientEGMs/A343220.wav');
d343220_normal_ecg = (smooth(dtrend(d343220_normal.data(:, 2)), 5));
d343220_normal_vt = (smooth(dtrend(d343220_normal.data(:, 7)), 5));

d228sr = importdata('/Users/amal029_old/PatientEGMs/A228a68.wav');
d228sr_ecg = (smooth(dtrend(d228sr.data(:, 2)), 5));
d228sr_vt = (smooth(dtrend(d228sr.data(:, 7)), 5));

d221sr = importdata('/Users/amal029_old/PatientEGMs/A221708.wav');
d221sr_ecg = (smooth(dtrend(d221sr.data(:, 2)), 5));
d221sr_vt = (smooth(dtrend(d221sr.data(:, 7)), 5));

%plot_new(1000,5000, d33093_normal_ecg, d33093_normal_vt, d343220_normal_ecg, d343220_normal_vt, d221sr_ecg, d221sr_vt);

i = 10000;
j = i*2;
train_input = (dtrend([d343220_normal_ecg(1:i); d33093_normal_ecg(1:i)]));
train_output = (dtrend([d343220_normal_vt(1:i); d33093_normal_vt(1:i)]));
%train_input = (dtrend([d343220_normal_ecg(1:i)]));
%train_output = (dtrend([d343220_normal_vt(1:i)]));

%test_input = (dtrend([d33093_normal_ecg(1:i)]));
%test_output = (dtrend([d33093_normal_vt(1:i)]));

MAX = 20;
algos = [string('trainlm'), string('trainscg')];
asize = size(algos);
neurons = [5:5:20, 40, 80, 100];
nsize = size(neurons);
results = cell(MAX*asize(2)*nsize(2),1);
max_avg = 0.0;
max_count = 1;
count = 1;
for delay = 1:MAX
    for n = 1:nsize(2)
        for a = 1:asize(2)
            
            %delay = 1;
    [X, wmx] = tonndata(train_input, false, false);
    [T, wtx] = tonndata(train_output, false, false);
    net = timedelaynet((1:delay), neurons(n), char(algos(a)));
    net.trainParam.showWindow = false;
    net.trainParam.showCommandLine = true;
    [Xs,Xi,Ai,Ts] = preparets(net,X,T);
    net = train(net,Xs,Ts,Xi,Ai);

% Now test it with the new input.
    s = 3000;
    e = 8000;

%figure();
%subplot(3, 1, 1);
% Validate
    [X, w1] = tonndata(d33093_normal_ecg(1:end), false, false);
%D = transpose(d33093_normal_ecg(1:delay));
    Ycap33093 = cell2mat(transpose(net(X)));
    C33093 = corr(Ycap33093, d33093_normal_vt);
%plot([Ycap33093(s:e), d33093_normal_vt(s:e)]);
%t = 'Male 1, seen in first part of training vector: ' + string(corr(Ycap33093, d33093_normal_vt));
%title(t);

%subplot(3, 1, 2);
% Validate
    Ycap343220 = cell2mat(transpose(net(tonndata(d343220_normal_ecg(1:end), false, false))));
    C343220 = corr(Ycap343220, d343220_normal_vt);
%plot([Ycap343220(s:e), d343220_normal_vt(s:e)]);
%t = 'Female 1, seen in second part of training vector: ' + string(corr(Ycap343220, d343220_normal_vt));
%title(t);

%subplot(3, 1, 3);
%Test new input
    Ycap221 = cell2mat(transpose(net(tonndata(d221sr_ecg(1:end), false, false))));
    C221 = corr(Ycap221, d221sr_vt);
%plot([Ycap221(s:e), d221sr_vt(s:e)]);
%t = 'Female 2, never seen before: ' + string(corr(Ycap221, d221sr_vt));
%title(t);

% Do the weighted average of the three correlations, we want to maximise
% that.
    avg = (C221+C33093+C343220)/3.0;
    if avg > max_avg
        max_avg = avg;
        max_count = count;
    end
    results(count) = {[avg, C221, C33093, C343220, neurons(n), delay, a]};
    count = count + 1;
        end
    end
end

% Final result.
[X, wmx1] = tonndata(train_input, false, false);
[T, wtx1] = tonndata(train_output, false, false);
net = timedelaynet(11, 15, 'trainlm');
net.trainParam.showWindow = false;
net.trainParam.showCommandLine = true;
[Xs,Xi,Ai,Ts] = preparets(net,X,T);
net = train(net,Xs,Ts,Xi,Ai);
figure();
subplot(3, 1, 1);
%Validate
[X, ww1] = tonndata(d33093_normal_ecg(1:end), false, false);
Ycap33093 = cell2mat(transpose(net(X)));
C33093 = corr(Ycap33093, d33093_normal_vt);
plot([Ycap33093(s:e), d33093_normal_vt(s:e)]);
t = 'Correlation with 33093, Male 65, no medication. Input ECG (Lead III) --> RVA (unipolar): ' + string(C33093);
title(t);
subplot(3, 1, 2);
%Validate
Ycap343220 = cell2mat(transpose(net(tonndata(d343220_normal_ecg(1:end), false, false))));
C343220 = corr(Ycap343220, d343220_normal_vt);
plot([Ycap343220(s:e), d343220_normal_vt(s:e)]);
t = 'correlation with 343220, Female 56, On amiodarone. Input ECG (Lead III) --> RVA (unipolar): ' + string(C343220);
title(t);
subplot(3, 1, 3);
%Test new input
Ycap221 = cell2mat(transpose(net(tonndata(d221sr_ecg(1:end), false, false))));
C221 = corr(Ycap221, d221sr_vt);
plot([Ycap221(s:e), d221sr_vt(s:e)]);
t = 'correlation with 221708 62 Female, Verapamil, Quinidine. Input ECG (Lead III) --> RVA (unipolar): ' + string(C221);
title(t);
