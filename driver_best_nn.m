% Prediction using radial basis function nueral net.
clear; clc;

% This is the 330-93 normal SR rhythm
d33093_normal = importdata('/Users/amal029_old/PatientEGMs/A330093.wav');
d33093_normal_ecg = (smooth(dtrend(d33093_normal.data(:, 3)), 5));
d33093_normal_vt = smooth(dtrend(d33093_normal.data(:, 7)), 5);

% This is the 343220 normal SR rhythm
d343220_normal = importdata('/Users/amal029_old/PatientEGMs/A343220.wav');
d343220_normal_ecg = (smooth(dtrend(d343220_normal.data(:, 2)), 5));
d343220_normal_vt = (smooth(dtrend(d343220_normal.data(:, 7)), 5));

d228sr = importdata('/Users/amal029_old/PatientEGMs/A228a68.wav');
d228sr_ecg = (smooth(dtrend(d228sr.data(:, 2)), 5));
d228sr_vt = (smooth(dtrend(d228sr.data(:, 7)), 5));

d221sr = importdata('/Users/amal029_old/PatientEGMs/A221708.wav');
d221sr_ecg = (smooth(dtrend(d221sr.data(:, 2)), 5));
d221sr_vt = (smooth(dtrend(d221sr.data(:, 7)), 5));

s = 3000;
e = 8000;

figure();
subplot(3, 1, 1);
% Validate
Ycap33093 = transpose(nnbest(transpose(d33093_normal_ecg(1:end)), transpose(d33093_normal_ecg(1:10))));
plot([Ycap33093(s:e), d33093_normal_vt(s:e)]);
t = 'Male 1, seen in first part of training vector: ' + string(corr(Ycap33093, d33093_normal_vt));
title(t);

subplot(3, 1, 2);
% Validate
Ycap343220 = transpose(nnbest(transpose(d343220_normal_ecg(1:end)), transpose(d343220_normal_ecg(1:10))));
plot([Ycap343220(s:e), d343220_normal_vt(s:e)]);
t = 'Female 1, seen in second part of training vector: ' + string(corr(Ycap343220, d343220_normal_vt));
title(t);

subplot(3, 1, 3);
%Test new input
Ycap221 = transpose(nnbest(transpose(d221sr_ecg(1:end)), transpose(d221sr_ecg(1:10))));
plot([Ycap221(s:e), d221sr_vt(s:e)]);
t = 'Female 2, never seen before: ' + string(corr(Ycap221, d221sr_vt));
title(t);