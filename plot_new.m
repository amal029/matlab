function plot_new(s, e, e1, v1, e2, v2, e3, v3)
subplot(3, 1, 1);
plot([e1(s: e), v1(s: e)]);
subplot(3, 1, 2);
plot([e2(s: e), v2(s: e)]);
subplot(3, 1, 3);
plot([e3(s: e), v3(s: e)]);