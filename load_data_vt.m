clear; clc

% This is the 330-93 normal SR rhythm
d33093_normal = importdata('/Users/amal029_old/PatientEGMs/A330093.wav');
d33093_normal_ecg = detrend(smooth(smooth(d33093_normal.data(:, 3), 'rloess'), 20));
d33093_normal_vt = detrend(smooth(smooth(d33093_normal.data(:, 7), 'rloess'), 5));
%d33093_normal_ecg = (d33093_normal.data(:, 3));
%d33093_normal_vt = (d33093_normal.data(:, 7));


% This is the 343220 normal SR rhythm
d343220_normal = importdata('/Users/amal029_old/PatientEGMs/A343220.wav');
d343220_normal_ecg = detrend(smooth(smooth(d343220_normal.data(:, 2), 'rloess'), 20));
d343220_normal_vt = detrend(smooth(smooth(d343220_normal.data(:, 7), 'rloess'), 5));
i = 5000;
j = i*2;
train_input = smooth(dtrend([d343220_normal_ecg(1:i); d33093_normal_ecg(1:i)]));
train_output = smooth(dtrend([d343220_normal_vt(1:i); d33093_normal_vt(1:i)]));

test_input = smooth(dtrend([d343220_normal_ecg(i+1:j); d33093_normal_ecg(i+1:j)]));
test_output = smooth(dtrend([d343220_normal_vt(i+1:j); d33093_normal_vt(i+1:j)]));

% The result of running the best fit model
load('general_best_new.mat');
subplot(2, 1, 1);
plot([sim(nlhw3, d33093_normal_ecg(6001:9500)), d33093_normal_vt(6001:9500)]);
c = corr([sim(nlhw3, d33093_normal_ecg(6001:9500)), d33093_normal_vt(6001:9500)]);
t = strcat('correlation with 33093, Male 65, no medication. Input ECG (Lead III) --> RVA (unipolar): '+string(c));
title(t(2,1));
subplot(2, 1, 2);
plot([sim(nlhw3, d343220_normal_ecg(6001:9500)), d343220_normal_vt(6001:9500)]);
c = corr([sim(nlhw3, d343220_normal_ecg(6001:9500)), d343220_normal_vt(6001:9500)]);
t = 'correlation with 343220, Female 56, On amiodarone. Input ECG (Lead III) --> RVA (unipolar): '+string(c);
title(t(2,1));

% This is the 241510 normal SR rhythm
%d241510_normal = importdata('/Users/amal029_old/PatientEGMs/A241510.wav');
%d241510_normal_ecg = smooth(smooth(d241510_normal.data(:, 2), 'rloess'), 20);
%d241510_normal_vt = smooth(smooth(d241510_normal.data(:, 7), 'rloess'), 5);

%plot_vt(1, 2000, d33093_normal.data, d343220_normal.data, d241510_normal.data, 3, 2, 2, 5);
%plot_vt(1, 2000, d33093_normal.data, d343220_normal.data, d241510_normal.data, 3, 2, 2, 7, 7, 7);
%plot_new(1000,5000, d33093_normal_ecg, d33093_normal_vt, d343220_normal_ecg, d343220_normal_vt, d241510_normal_ecg, d241510_normal_vt);